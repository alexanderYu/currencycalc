package alex.test.currencyCalc;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import alex.test.currencyCalc.services.Calculator;
import org.jdesktop.swingx.JXDatePicker;

public class Main {
    private static final String LABLE_PROFIT = "Profit RUB";
    private static final String LABLE_LOSS = "Loss RUB";

    private static Calculator calculator = new Calculator();
    private static JButton button = new JButton("Recalculate");
    private static JXDatePicker picker = new JXDatePicker();
    private static JLabel labelDate = new JLabel("Date");
    private static JLabel labelAmount = new JLabel("Amount USD");
    private static NumberFormat currencyFormatUSD = getCurrencyFormatUSD();
    private static NumberFormat currencyFormatRUB = getCurrencyFormatRUB();
    private static JFormattedTextField currencyAmount = getCurrencyAmount();
    private static JLabel labelResult = new JLabel("Profit RUB");
    private static JTextField profit = new JTextField("", 5);

    private static JFrame getJFrame() {
        JFrame jFrame= new JFrame("Currency Calculator");
        jFrame.setVisible(true);

        jFrame.setBounds(100,100,350,150);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = jFrame.getContentPane();
        container.setLayout(new GridLayout(4,3,3,3));

        picker.setDate(Calendar.getInstance().getTime());
        picker.setFormats(new SimpleDateFormat("dd.MM.yyyy"));

        container.add(labelDate);
        container.add(picker);

        container.add(labelAmount);
        container.add(currencyAmount);

        container.add(labelResult);
        profit.setEditable(false);
        container.add(profit);

        button.addActionListener(new ButtonEventListener());
        container.add(button);

        return jFrame;
    }

    private static JFormattedTextField getCurrencyAmount() {
        NumberFormatter formatter = new NumberFormatter(currencyFormatUSD);
        formatter.setMaximum(1000000000000.00);
        formatter.setMinimum(0.00);
        formatter.setAllowsInvalid(false);
        formatter.setOverwriteMode(true);
        JFormattedTextField field = new JFormattedTextField(formatter);
        field.setValue(100.00);
        return field;
    }

    private static NumberFormat getCurrencyFormatUSD() {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        format.setMaximumFractionDigits(2);
        return format;
    }

    private static NumberFormat getCurrencyFormatRUB() {
        NumberFormat format = NumberFormat.getCurrencyInstance();
        format.setMaximumFractionDigits(2);
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) format).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        ((DecimalFormat) format).setDecimalFormatSymbols(decimalFormatSymbols);
        return format;
    }
    static class ButtonEventListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            Double amount = (Double) currencyAmount.getValue();
            Date date = picker.getDate();
            Double result = null;
            try {
                result= calculator.recalculate(amount, date);
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
            if (result >= 0) {
                labelResult.setText(LABLE_PROFIT);
            }
            else {
                labelResult.setText(LABLE_LOSS);
                result = Math.abs(result);
            }
            profit.setText(currencyFormatRUB.format(result));
        }
    }

    public static void main(String[] args) {
        JFrame jFrame = getJFrame();
    }
}
