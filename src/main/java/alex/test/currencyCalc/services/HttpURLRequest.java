package alex.test.currencyCalc.services;

import alex.test.currencyCalc.dto.CurrencyDto;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class HttpURLRequest {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private static final String URL_API = "http://data.fixer.io/api/";
    private static final String URL_LATEST = "latest?";
    private static final String URL_KEY = "access_key=";
    private static final String URL_BASE = "base=USD";
//    private static final String URL_BASE = "base=EUR";

    public CurrencyDto sendGetLatest() throws Exception {
        return sendGet(null);
    }

    public CurrencyDto sendGet(Date date) throws Exception {
        String url = URL_API;
        if (null == date) {
            url = url + URL_LATEST;
        }
        else {
            url = url + sdf.format(date) + "?";
        }

        PropertiesGetter propertiesGetter = new PropertiesGetter();
        Properties prop = propertiesGetter.getProp();
        url = url + URL_KEY + prop.getProperty("data.fixer.key") + "&"
        + URL_BASE;

        URL obj = new URL(url);
        java.net.HttpURLConnection con = (java.net.HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        int responseCode = con.getResponseCode();

        if (200 != responseCode) {
            throw new Exception("Unexpected response code of remote service: " + responseCode);
        }

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        ObjectMapper mapper = new ObjectMapper();
        CurrencyDto currencyDto = mapper.readValue(response.toString(), CurrencyDto.class);

        return currencyDto;
    }

}