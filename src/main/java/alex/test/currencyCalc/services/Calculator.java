package alex.test.currencyCalc.services;

import alex.test.currencyCalc.dto.CurrencyDto;
import org.apache.commons.lang3.BooleanUtils;

import java.util.Date;

public class Calculator {

    private static final String RUB = "RUB";

    private static final Float SPREAD = 0.005f;

    private static final HttpURLRequest httpReq = new HttpURLRequest();

    public Double recalculate(Double amount, Date date) throws Exception {
        if (date.after(new Date())) {
            throw new Exception("Incorrect input date");
        }

        CurrencyDto latest = httpReq.sendGetLatest();
        validateDto(latest);
        Double latestRate = latest.getRates().get(RUB);

        CurrencyDto historical = httpReq.sendGet(date);
        validateDto(historical);
        Double historicalRate = historical.getRates().get(RUB);

        Double buy = amount * historicalRate * (1 + SPREAD);
        Double sell = amount * latestRate * (1 - SPREAD);

        return sell - buy;
    }

    private void validateDto(CurrencyDto dto) throws Exception {
        if (null == dto || BooleanUtils.isNotTrue(dto.getSuccess())) {
            String error = null;
            if (null != dto.getError()) {
                error = " " + String.valueOf(dto.getError());
            }
            throw new Exception("ERROR: API answer invalid." + error);
        }
        if (null == dto.getRates() || null == dto.getRates().get(RUB)) {
            throw new Exception("ERROR: API answer invalid. Rate is ubsent");
        }
    }
}
