package alex.test.currencyCalc.services;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesGetter {

    public Properties getProp() throws IOException {

        FileInputStream fis;
        Properties property = new Properties();

        try {
            fis = new FileInputStream("src/main/resources/config.properties");
            property.load(fis);

        } catch (IOException e) {
            throw new IOException("ERROR: Propertyes file is ubsent");
        }
        return property;
    }

}